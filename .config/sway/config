# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
#set $term kitty
set $term foot
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
#set $menu dmenu_path | dmenu | xargs swaymsg exec --
set $menu fuzzel

set $browser firefox

set $wallpaper ~/.cache/wallpaper

# Gaps Settings
default_border pixel 1

gaps top 2
gaps inner 10

# Thin borders:
smart_borders on

### Appearance SwayFX specific
# window corner radius in px
corner_radius 10

# Window background blur
blur on
blur_xray off
blur_passes 3
blur_radius 5

# inactive window fade amount. 0.0 = no dimming, 1.0 = fully dimmed
default_dim_inactive 0.5
dim_inactive_colors.unfocused #000000FF
dim_inactive_colors.urgent #900000FF


### End SwayFX specific


### Output configuration
# Set wallpaper:
exec swaybg -i $wallpaper
output * bg $wallpaper fill

output eDP-1 pos 1920 0 res 1920x1080
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
exec swayidle -w \
        timeout 300 'swaylock -f -i $wallpaper' \
        timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
        before-sleep 'swaylock -f -i $wallpaper'

# Start Polkit
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
exec mako
exec yambar -c ~/.config/yambar/sway/config.yml
#exec pipewire &
exec blueman-applet &
exec blueman-manager &
exec wlsunset -l 52.9 -L 6.5 &

#exec /usr/libexec/polkit-gnome-authentication-agent-1
### Input configuration
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.
input type:keyboard xkb_numlock enabled
input type:touchpad natural_scroll enabled

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+q kill

    # Start your launcher
    bindsym $mod+d exec $menu
    
    bindsym Shift+$mod+b exec $browser
    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'

    
    # Take a screenshot of the focused output and save it into screenshots
    bindsym Print exec ~/.config/sway/scripts/screenshot.sh
    #bindsym Print exec grim -o $(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name') -t jpeg ~/Pictures/Screenshots/$(date +%Y-%m-%d_%H-%m-%s).jpg

    # Take a screenshot of the selected region
    bindsym $mod+Print exec grim -t jpeg -g "$(slurp)" ~/Pictures/Screenshots/$(date +%Y-%m-%d_%H-%m-%s).jpg

    # Take a screenshot and save it to the clipboard
    bindsym $mod+Shift+Print exec grim -g "$(slurp -d)" - | wl-copy

    # Open power menu
    bindsym Shift+$mod+p exec ~/.local/bin/powermenu

    #Toggle bar
    bindsym $mod+y exec "killall yambar || ~/.config/yambar/scripts/yambar-sway-start.sh"


#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

    bindgesture swipe:right workspace prev
    bindgesture swipe:left workspace next
    #bindgesture swipe:down:3 
    #bindgesture swipe:up:3
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"
bindsym $mod+x exec "swaylock -f -i $wallpaper"
bindsym --locked XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +3%
bindsym --locked XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -3%
bindsym --locked XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle
# Window rules
for_window [app_id="mpv"] floating enable, resize set width 1200 height 800, focus

focus_on_window_activation focus

# Assign applications to specific workspaces
#for_window [app_id="foot"] move window to workspace 2, focus
for_window [class="Spotify"] move window to workspace 9
for_window [app_id="blueman-manager"] move window to workspace 9
for_window [app_id="nvim"] move window to workspace 5
for_window [app_id="Firefox"] move window to workspace 1, focus
for_window [app_id="aerc"] move window to workspace 2
for_window [app_id="org.qutebrowser.qutebrowser"] move window to workspace 1, focus
for_window [app_id="ranger"] move window to workspace 6
for_window [app_id="thunar"] move window to workspace 6
for_window [app_id="tut"] move window to workspace 7
for_window [app_id="Thunderbird"] move window to workspace 2
for_window [app_id="mpv"] move window to workspace 4


#bar {
#    swaybar_command waybar
#}
include /etc/sway/config.d/*
