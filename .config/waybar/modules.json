//  __  __           _       _            
// |  \/  | ___   __| |_   _| | ___  ___  
// | |\/| |/ _ \ / _` | | | | |/ _ \/ __| 
// | |  | | (_) | (_| | |_| | |  __/\__ \ 
// |_|  |_|\___/ \__,_|\__,_|_|\___||___/ 
//                                        
//  
// by Stephan Raabe (2023) 
// ----------------------------------------------------- 
//
{
    // Workspaces
    "hyprland/workspaces" : {
        "on-click": "activate",
        "disable-scroll": true,
        "all-outputs": false,
        "format": "{}",
        "format-icons": {
			"urgent": "",
			"active": "",
			"default": ""
        },    
    },

    // Taskbar
    "wlr/taskbar": {
        "format": "{icon}",
        "icon-size": 18,
        "tooltip-format": "{title}",
        "on-click": "activate",
        "on-click-middle": "close",
        "ignore-list": [
           "kitty",
           "foot"
        ],
        "app_ids-mapping": {
            "firefoxdeveloperedition": "firefox-developer-edition"
        },
        "rewrite": {
            "Firefox Web Browser": "Firefox",
            "Foot Server": "Terminal"
        }
    },

    // Hyprland Window
    "hyprland/window": {
        "rewrite": {
            "(.*) - Brave": "$1",
            "(.*) - Chromium": "$1",
            "(.*) - Brave Search": "$1",
            "(.*) - Outlook": "$1",
            "(.*) Microsoft Teams": "$1"
        },
        "separate-outputs": true
    },

    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": " {name} {icon}",
        "format-icons": {
            "locked": "",
            "unlocked": ""
        }
    },

    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "format-icons": ["", ""]
    },
    
    // Wallpaper
    "custom/wallpaper": {
        "format": "",
        "on-click": "~/.config/hypr/scripts/wallpaper.sh select",
        "on-click-right": "~/dotfiles/hypr/scripts/wallpaper.sh",
        "tooltip": false
    },

    // Waybar Themes
    "custom/waybarthemes": {
        "format": "",
        "on-click": "~/.config/waybar/themeswitcher.sh",
        "tooltip": false
    },


    // Keybindings
    "custom/keybindings": {
        "format": "",
        "on-click": "~/.config/hypr/scripts/keybindings.sh",
        "tooltip": false
    },

    // Filemanager Launcher
    "custom/filemanager": {
        "format": "",
        "on-click": "~/.config/waybar/scripts/filemanager.sh",
        "tooltip": false
    },

    // Outlook Launcher
    "custom/outlook": {
        "format": "",
        "on-click":"vivaldi --app=https://outlook.office.com/mail/",
        "tooltip": false
    },

    // GMail Launcher
    "custom/gmail": {
        "format": "",
        "on-click":"vivaldi --app=https://mail.google.com/mail/u/0/#inbox",
        "tooltip": false
    },  

    // Teams Launcher
    "custom/teams": {
        "format": "",
        "on-click": "vivaldi --app=https://teams.microsoft.com/go",
        "tooltip": false
    },

    // Browser Launcher
    "custom/browser": {
        "format": "",
        "on-click": "~/.config/waybar/scripts/browser.sh",
        "tooltip": false
    },    


    // Calculator
    "custom/calculator": {
        "format": "",
        "on-click": "qalculate-gtk",
        "tooltip": false
    },


    // Power Menu
    "custom/exit": {
        "format": "",
        "on-click": "wlogout",
        "tooltip": false
    },

    // Keyboard State
    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": "{name} {icon}",
        "format-icons": {
            "locked": "",
            "unlocked": ""
        }
    },

    // System tray
    "tray": {
        "icon-size": 21,
        "spacing": 10
    },

    // Clock
    "clock": {
        // "timezone": "America/New_York",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        // START CLOCK FORMAT
        "format-alt": "{:%Y-%m-%d}"
        // END CLOCK FORMAT
    },

    // System
    "custom/system": {
        "format": "",
        "tooltip": false
    },

    // CPU
    "cpu": {
        "format": "/ C {usage}% ",
        "on-click": "foot -e btop"
    },

    // Memory
    "memory": {
        "format": "/ M {}% ",
        "on-click": "foot -e btop"
    },

    // Harddisk space used
    "disk": {
        "interval": 30,
        "format": "D {percentage_used}% ",
        "path": "/",
        "on-click": "foot -e btop"
    }, 



    // Group Hardware
    "group/hardware": {
        "orientation": "inherit",
        "drawer": {
            "transition-duration": 300,
            "children-class": "not-memory",
            "transition-left-to-right": false
        },        
        "modules": [
            "custom/system",
            "disk",
            "cpu",
            "memory"
        ]
    },
    // Group Battery
    "group/battery": {
        "orientation": "inherit",
        "drawer": {
            "transition-duration": 300,
            "transition-left-to-right": false
        },        
        "modules": [
            "custom/power",
            "battery",
            "battery#bat1"
    ]
    },
    
    // Group Settings
    "group/settings": {
        "orientation": "horizontal",
        "modules": [
            "custom/settings",
            "custom/waybarthemes",
            "custom/wallpaper"
        ]
    },

    // Group Quicklinks
    "group/quicklinks": {
        "orientation": "horizontal",
        "modules": [
            "custom/browser",
            "custom/filemanager",
            "custom/outlook",
            "custom/gmail",     
         ]
    },

    // Network
    "network": {
        "format": "{ifname}",
        "format-wifi": "   {signalStrength}%",
        "format-ethernet": "  {ifname}",
        "format-disconnected": "Disconnected",
        "tooltip-format": " {ifname} via {gwaddri}",
        "tooltip-format-wifi": "  {ifname} @ {essid}\nIP: {ipaddr}\nStrength: {signalStrength}%\nFreq: {frequency}MHz\nUp: {bandwidthUpBits} Down: {bandwidthDownBits}",
        "tooltip-format-ethernet": " {ifname}\nIP: {ipaddr}\n up: {bandwidthUpBits} down: {bandwidthDownBits}",
        "tooltip-format-disconnected": "Disconnected",
        "max-length": 50,
        "on-click": "~/dotfiles/.settings/networkmanager.sh"
    },
    // Power
    "custom/power": {
        "format": "  ",
        "tooltip": false
    },

    // Battery
    "battery": {
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{icon}   {capacity}%",
        "format-charging": "  {capacity}%",
        "format-plugged": "  {capacity}%",
        "format-alt": "{icon}  {time}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": [" ", " ", " ", " ", " "]
    },
    "battery#bat1": {
        "bat": "BAT1",
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{icon}   {capacity}%",
        "format-charging": "  {capacity}%",
        "format-plugged": "  {capacity}%",
        "format-alt": "{icon}  {time}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": [" ", " ", " ", " ", " "]    
    },

    // Pulseaudio
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{icon} {volume}%",
        "format-bluetooth": "{volume}% {icon}  ",
        "format-bluetooth-muted": " {icon}  ",
        "format-muted": " {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", " ", " "]
        },
        "on-click": "pavucontrol"
    },

    // Updates Count
    "custom/updates": {
        "format": "  {}",
        "tooltip-format": "{}",
        "escape": true,
        "return-type": "json", 
        "restart-interval": 60,
        "exec": "~/.config/waybar/scripts/arch-updates.sh",
        "on-click": "foot -e paru; pkill -SIGRTMIN+8 waybar",
        "signal": 8,
        "tooltip": false
    },

    // Bluetooth
    "bluetooth": {
        "format": " {status}",
        "format-disabled": "",
        "format-off": "",
        "interval": 30,
        "on-click": "blueman-manager"
    },
    "custom/launcher":{
        "format": "  ",
        "on-click": "fuzzel",
        "tooltip": false    
    },  
    // Idle Inhibator
    "idle_inhibitor": {
        "format": "{icon}",
        "tooltip": true,
        "format-icons":{
            "activated": "",
            "deactivated": ""
        },
        "on-click-right": "hyprlock"        
    }
}
