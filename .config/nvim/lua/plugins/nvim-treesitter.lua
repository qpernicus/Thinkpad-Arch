return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		dependencies = {
			"windwp/nvim-ts-autotag",
		},
		config = function()
			-- import nvim-treesitter plugin
			local treesitter = require("nvim-treesitter.configs")

			-- configure treesitter
			treesitter.setup({ -- enable syntax highlighting
				lazy = false,
				highlight = {
					enable = true,
				},
				-- enable indentation
				indent = { enable = true },
				-- enable autotagging (w/ nvim-ts-autotag plugin)
				autotag = { enable = true },
				-- ensure these language parsers are installed
				--ensure_installed = {
				--	"json",
				--	"yaml",
				--	"markdown",
				--	"markdown_inline",
				--	"bash",
				--	"lua",
				--	"vim",
				--	"python",
				--},
				-- enable nvim-ts-context-commentstring plugin for commenting tsx and jsx
				--context_commentstring = {
				--	enable = true,
				--	enable_autocmd = false,
				--},
				-- auto install above language parsers
				auto_install = true,
			})
		end,
	},
}
