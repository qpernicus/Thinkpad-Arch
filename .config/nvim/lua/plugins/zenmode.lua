return {
    -- add symbols-outline
    {
      "folke/zen-mode.nvim",
      dependencies = { "folke/twilight.nvim" },
      cmd = "ZenMode",
      keys = { { "<ENTER>", "<cmd>:ZenMode<cr>", desc = "ZenMode" } },
    },
  }