return {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000,
	config = function()
		vim.g.edge_transparent_background = 1
		vim.cmd.colorscheme("catppuccin-mocha")
		require("catppuccin").setup({
			integrations = {
				cmp = true,
				alpha = true,
				gitsigns = true,
				nvimtree = true,
				mason = true,
				noice = true,
				which_key = true,
				telescope = {
					enabled = true,
					-- style = "nvchad"
				},
				treesitter = true,
				notify = false,
				mini = {
					enabled = true,
					indentscope_color = "lavender",
				},
			},
		})
	end,
}
-- Edge theme
