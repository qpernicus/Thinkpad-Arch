#!/bin/bash

# Swayidle state
function state {
	if pgrep "swayidle" > /dev/null
	then
		"  "
	else
		"  "
	fi
}

case $1 in
	toggle)
		toggle
		;;
	*)
		if pgrep "swayidle" > /dev/null
		then
			icon="Swayidle Active"
		else
			icon="Swayidle Inactive"
		fi
		printf "%s" "$icon "
		;;
esac

