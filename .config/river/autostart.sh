#!/bin/bash

# Authentication dialog

pkill -f /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Kill any existing pipewire / wireplumber daemons and only then try to start a new set.

# pkill -u "${USER}" -x pipewire\|wireplumber 1>/dev/null 2>&1
# dbus-run-session pipewire &> /dev/null &

# Start xdg-desktop-portal-wlr  

pkill -f /usr/libexec/xdg-desktop-portal-wlr
/usr/libexec/xdg-desktop-portal-wlr &

# Start Kanshi which also starts Yambar
pkill -f kanshi
kanshi &

read -r wallpaper<~/.cache/wallpaper
pkill -f swaybg
swaybg -m fill -i "$wallpaper" &

pkill -f mako
mako &

pkill -f wlsunset
wlsunset -l 52.9 -L 6.5 &

export wallpaper=$( echo $wallpaper)

pkill -f swayidle
swayidle -w \
	timeout 300 'swaylock -f -i $wallpaper' \
	timeout 600 'wlopm --off \*;swaylock -F -i $wallpaper' resume 'wlopm --on \*' \
	before-sleep 'swaylock -f -i $wallpaper' &
