# Dotfiles for my [SwayFX](https://github.com/WillPower3309/swayfx) , [River](https://github.com/riverwm/river) and [Hyprland](https://github.com/hyprwm/Hyprland) setup.

My Hyprland setup incl Eww-bar
![ScreenShot](screenshot.jpg)

Current Wallpaper:
![Wallpaper](wallpaper.jpg)

## Details

Below is a list of some of the packages that I use for my current setup.<br />

- **OS** --- [Arch](https://www.Archlinux.org/)
- **WM** --- [SwayFX](https://github.com/WillPower3309/swayfx) , [River](https://github.com/riverwm/river) and [Hyprland](https://github.com/hyprwm/Hyprland)
- **Status Bar** --- [Yambar](https://codeberg.org/dnkl/yambar) & [Eww]
- **Screen Locker** --- [Swaylock](https://github.com/swaywm/swaylock) & [Hyprlock]
- **Screenshots** --- [Grim](https://aur.archlinux.org/packages/grim-git)
  --- [Slurp](https://aur.archlinux.org/packages/slurp-git)
- **Idle Management Daemon** --- [Swayidle](https://aur.archlinux.org/packages/swayidle.git) & [Hypridle]
- **Shell** --- Trying out [ZSH](https://www.zsh.org/) besides [Fish](https://fishshell.com/) both using [Starship](https://starship.rs/)
- **Terminal** --- [Foot](https://codeberg.org/dnkl/foot) & Kitty
- **Notification Daemon** --- [Mako](https://github.com/emersion/mako)
- **Application Launcher** --- [Fuzzel](https://codeberg.org/dnkl/fuzzel)
- **Image Viewer** --- [Imv](https://sr.ht/~exec64/imv/)
- **Editor** --- [Neovim](https://neovim.io/)
- **Web Browser** --- [Qutebrowser](https://www.qutebrowser.org) & [Vivaldi]
- **PDF Viewer** --- [Zathura](https://pwmt.org/projects/zathura/)
- **Video player** --- [Mpv](https://mpv.io/)
